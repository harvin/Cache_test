package com.harvin;


public class Main {
	
	public static void main(String[] args){
//		Integer i1 = new Integer(1);
//		Integer i2 = new Integer(1);
//		System.out.println(i1 == i2);
//		
//		Integer i3 = 1;
//		Integer i4 = 1;
//		System.out.println(i3 == i4);
//		
//		Integer i5 = 200;
//		Integer i6 = 200;
//		System.out.println(i5 == i6);
		
		Cache_test ct1 = new Cache_test("test1");
		Cache_test ct2 = new Cache_test("test1");
		//由于这里都是直接创建，所以下面会输出false;
		System.out.println(ct1 == ct2);
		
		Cache_test ct3 = Cache_test.valueOf("test2");
		Cache_test ct4 = Cache_test.valueOf("test2");
		//由于这里使用的是valueOf()函数，将会使用到缓存。所以下面输出true.
		System.out.println(ct3 == ct4);
	}
}
