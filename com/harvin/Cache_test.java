package com.harvin;

/*
 * <p>
 * 该对象使用数组实现了缓存，也就是，
 * 每一次使用valueOf()创建新对象时，系统将会确认缓存中是否已经存在相应的对象（即data相等）。
 * 假如存在，则直接返回缓存已存在的对象；
 * 假如不存在，则创建一个新对象，存储到缓存中，并返回新创建的对象。
 * </p>
 * 
 * @author Harvin.
 * @version 1.0
 */
public class Cache_test {
	//需要存储的数据
	private final String data;
	
	public Cache_test(String data){
		this.data = data;
	}
	public String get_data(){
		return this.data;
	}
	@Override
	//直接判断是否是指向同一个对象
	public boolean equals(Object obj){
		if (this == obj) {
			return true;
		}
		return false;
	}
	
	
	//定义缓存的大小
	private final static int MAX_SIZE = 10;
	//使用数组来存储缓存
	private static Cache_test[] cache
	= new Cache_test[MAX_SIZE];
	//定义当前缓存存储的位置
	private static int pos = 0;
	
	/* 判断是否已经缓存了包含该data对象的Cache_test对象，
	 * 如果存在，则直接返回;
	 * 如果不存在，则直接创建后再将其返回
	 */
	public static Cache_test valueOf(String data){
		for (int i = 0; i < MAX_SIZE; i++) {
			if (cache[i] != null
					&& cache[i].get_data().equals(data)) {
				return cache[i];
			}
		}
		if(MAX_SIZE == pos){
			cache[0]	= new Cache_test(data);
			pos			= 1;
		}else{
			cache[pos]	= new Cache_test(data);
		}
		return cache[pos++];
	}
}
